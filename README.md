# Frontend Interview Question

You have given a function called `getAutoCompleteResults()` and it takes the query and returns a promise.
The promise returns the filter value based on query passed

```ts
return new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(
      fruits.filter((fruit) =>
        fruit.toLowerCase().includes(query.toLocaleLowerCase()),
      ),
    );
  }, Math.random() * 1000);
});
```

the above code returns a promise which resolves after 1 second and returns the filtered value based on the query passed. The `Math.random()` is used to simulate the network latency.

### You have to make a search functionality:

- which does not search immediately but it holds for 500ms before perform the search.
- only top 5 search results should appear in ascending order.
