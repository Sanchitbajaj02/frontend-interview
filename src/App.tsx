import React from "react";

function getAutoCompleteResults(query: string): Promise<string[]> {
  const fruits = [
    "Apple",
    "Banana",
    "Orange",
    "Kiwi",
    "Pineapple",
    "Strawberry",
    "Watermelon",
    "Mango",
    "Grape",
    "Lemon",
    "Lime",
    "Peach",
    "Pear",
    "Raspberry",
    "Blueberry",
    "Blackberry",
    "Cherry",
    "Coconut",
    "Cranberry",
    "Cucumber",
    "Grapefruit",
    "Honeydew",
    "Jackfruit",
    "Mandarin",
    "Melon",
    "Nectarine",
    "Papaya",
    "Pomegranate",
    "Rambutan",
  ];

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(
        fruits.filter((fruit) =>
          fruit.toLowerCase().includes(query.toLocaleLowerCase()),
        ),
      );
    }, Math.random() * 1000);
  });
}

function App() {
  return (
    <>
      <div className="w-full h-screen flex flex-col items-center bg-gray-900">
        <span className="sr-only">Search</span>

        <input
          type="search"
          className="mt-24 mb-4 placeholder:italic placeholder:text-gray-400 max-w-lg w-full px-4 py-2 rounded-md bg-gray-700 text-slate-200 focus:outline-none focus:ring-1 focus:ring-slate-500 focus:border-transparent "
          name="search"
          placeholder="search for fruits..."
        />
        <div className="text-gray-300 flex flex-col gap-2 items-start">
          Show results here
        </div>
      </div>
    </>
  );
}

export default App;
